﻿namespace Reflection
{
    public class Numbers
    {
        public int Num1;

        int _num2;

        public int Num3 { get; set; }

        int _num4 { get; set; }

        int _num5 { get; set; }

        public Numbers()
        {
            Num1 = 1;
            _num2 = 2;
            Num3 = 3;
            _num4 = 4;
            _num5 = 5;
        }

        public override string ToString()
        {
            return $"num1: {Num1} num2: {_num2} num3: {Num3} num4: {_num4} num5: {_num5}";
        }
    }
}
