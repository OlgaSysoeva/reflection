﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Reflection
{
    public static class Serializer<T> where T : class, new()
    {
        public static string Serialise(T cls)
        {
            if (cls == null)
            {
                throw new ArgumentNullException(nameof(cls));
            }

            StringBuilder strData = new StringBuilder();

            var propCls = cls.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            var fldCls = cls.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            foreach (var item in fldCls)
            {
                strData.Append($"{item.Name}:{item.GetValue(cls)},");
            }

            foreach (var item in propCls)
            {
                strData.Append($"{item.Name}:{item.GetValue(cls)},");
            }

            return strData.ToString();
        }

        public static T Deserialise(string strData)
        {
            if (string.IsNullOrEmpty(strData))
            {
                throw new ArgumentNullException();
            }

            T clsData = new T();

            var strMas = strData.Split(',');

            foreach (var item in strMas)
            {
                var mas = item.Split(':');
                
                if (mas.Count() != 2)
                {
                    throw new TargetParameterCountException(mas.ToString());
                }

                MemberInfo memberInfo = clsData
                    .GetType()
                    .FindMembers(MemberTypes.Field | MemberTypes.Property,
                        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                        new MemberFilter(Search),
                        mas[0])
                    .FirstOrDefault();

                if (memberInfo == null)
                {
                    continue;
                }

                if (memberInfo.MemberType == MemberTypes.Property)
                {
                    var typeObj = ((PropertyInfo)memberInfo).PropertyType;
                    var value = Convert.ChangeType(mas[1], typeObj);

                    ((PropertyInfo)memberInfo).SetValue(clsData, value);
                }
                else if (memberInfo.MemberType == MemberTypes.Field)
                {
                    var typeObj = ((FieldInfo)memberInfo).FieldType;
                    var value = Convert.ChangeType(mas[1], typeObj);

                    ((FieldInfo)memberInfo).SetValue(clsData, value);
                }
            }
            
            return clsData;
        }

        public static bool Search(MemberInfo info, Object obj)
        {

            return info.Name.ToString() == obj.ToString();
        }
    }
}
