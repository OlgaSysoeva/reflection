﻿using System;
using System.Diagnostics;

using Newtonsoft.Json;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            var cls = new Numbers()
            {
                Num1 = 6,
                Num3 = 8
            };

            try
            {
                var serData = CustomSerialiser(cls);

                CustomDeserialiser(serData);

                var jsonSer = JsonSerialize(cls);

                JsonDeserialize(jsonSer);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        static string CustomSerialiser(Numbers cls)
        {
            Console.WriteLine("\nСериализация:");

            if (cls == null)
            {
                throw new ArgumentNullException(nameof(cls));
            }

            var colReplay = 10000;
            string serdata = "";

            Stopwatch stopwatchSer = new Stopwatch();
            stopwatchSer.Start();

            for (int i = 1; i <= colReplay; i++)
            {
                serdata = Serializer<Numbers>.Serialise(cls);
            }

            stopwatchSer.Stop();

            ConsoleWrite(colReplay, stopwatchSer, serdata);

            return serdata;
        }

        static void CustomDeserialiser(string strData)
        {
            Console.WriteLine("\nДесериализация:");

            if (string.IsNullOrEmpty(strData))
            {
                throw new ArgumentNullException();
            }

            var colReplay = 10000;
            Numbers deserData = null;

            Stopwatch stopwatchDeser = new Stopwatch();
            stopwatchDeser.Start();

            for (int i = 1; i <= colReplay; i++)
            {
                deserData = Serializer<Numbers>.Deserialise(strData);
            }

            stopwatchDeser.Stop();

            ConsoleWrite(colReplay, stopwatchDeser, deserData);
        }

        static string JsonSerialize(Numbers cls)
        {
            Console.WriteLine("\nСериализация NewtonsoftJson:");

            if (cls == null)
            {
                throw new ArgumentNullException(nameof(cls));
            }

            var colReplay = 10000;
            string serdata = "";

            Stopwatch stopwatchSer = new Stopwatch();
            stopwatchSer.Start();

            for (int i = 1; i <= colReplay; i++)
            {
                serdata = JsonConvert.SerializeObject(cls);
            }

            stopwatchSer.Stop();

            ConsoleWrite(colReplay, stopwatchSer, serdata);

            return serdata;
        }

        static void JsonDeserialize(string strData)
        {
            Console.WriteLine("\nДесериализация NewtonsoftJson:");

            if (string.IsNullOrEmpty(strData))
            {
                throw new ArgumentNullException();
            }

            var colReplay = 10000;
            Numbers deserData = null;

            Stopwatch stopwatchDeser = new Stopwatch();
            stopwatchDeser.Start();

            for (int i = 1; i <= colReplay; i++)
            {
                deserData = JsonConvert.DeserializeObject<Numbers>(strData);
            }

            stopwatchDeser.Stop();

            ConsoleWrite(colReplay, stopwatchDeser, deserData);
        }

        static void ConsoleWrite(int colReplay, Stopwatch stopwatch, object data)
        {
            Console.WriteLine($"Количество повторов: {colReplay}");

            Console.WriteLine("Затрачено миллисекунд на выполнение: " + stopwatch.ElapsedMilliseconds);

            Console.WriteLine(data);
        }
    }
}
